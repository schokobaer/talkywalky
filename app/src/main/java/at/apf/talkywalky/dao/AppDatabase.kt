package at.apf.talkywalky.dao

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.model.Walker
import at.apf.talkywalky.model.Invitation


@Database(entities = [Walker::class, Channel::class, Invitation::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun walkerDao(): WalkerDao

    abstract fun channelDao(): ChannelDao

    abstract fun invitationDao(): InvitationDao

    fun contactRepo(): ContactRepo {
        return ContactRepo(channelDao(), walkerDao())
    }

    companion object {

        @Volatile
        private var instance: AppDatabase? = null

        fun getDb(ctx: Context): AppDatabase {
            if (instance == null) {
                synchronized(AppDatabase::class.java) {
                    if (instance == null) {
                        instance = Room.databaseBuilder(
                            ctx.getApplicationContext(),
                            AppDatabase::class.java!!, "app_database"
                        )
                            //.addMigrations(MIGRATION_1_2)
                            .build()
                    }
                }
            }
            return instance!!
        }

        /*private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                // Since we didn't alter the table, there's nothing else to do here.
                database.execSQL("CREATE TABLE invitation (secret TEXT, challenge TEXT, expireDate )")
            }
        }*/
    }
}