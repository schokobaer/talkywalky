package at.apf.talkywalky.dao

import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.model.Walker
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

class ContactRepo(val channelDao: ChannelDao, val walkerDao: WalkerDao) {

    fun getAll(): Single<List<Contact>> {
        return Single.zip(walkerDao.getAll(), channelDao.getAll(), BiFunction { walker: List<Walker>, channels: List<Channel> ->
            val result = ArrayList<Contact>()
            result.addAll(walker)
            result.addAll(channels)
            result.sortByDescending { c -> c.lastAccess }
            return@BiFunction result
        })
    }

    fun getBlacklist(): Single<List<Contact>> {
        return Single.zip(walkerDao.getBlacklist(), channelDao.getBlacklist(), BiFunction { walker: List<Walker>, channels: List<Channel> ->
            val result = ArrayList<Contact>()
            result.addAll(walker)
            result.addAll(channels)
            result.sortBy { c -> c.name }
            return@BiFunction result
        })
    }

    fun create(contact: Contact): Completable {
        return when (contact) {
            is Walker -> walkerDao.create(contact)
            is Channel -> channelDao.create(contact)
            else -> return Completable.error(ClassNotFoundException())
        }
    }

    fun update(contact: Contact): Completable {
        return when (contact) {
            is Walker -> walkerDao.update(contact)
            is Channel -> channelDao.update(contact)
            else -> return Completable.error(ClassNotFoundException())
        }
    }

    fun delete(contact: Contact): Completable {
        return when (contact) {
            is Walker -> walkerDao.delete(contact)
            is Channel -> channelDao.delete(contact)
            else -> return Completable.error(ClassNotFoundException())
        }
    }
}