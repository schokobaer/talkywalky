package at.apf.talkywalky.dao

import androidx.room.*
import at.apf.talkywalky.model.Channel
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface ChannelDao {

    @Query("SELECT * FROM channel c ORDER BY c.lastAccess DESC")
    fun getAll(): Single<List<Channel>>

    @Query("SELECT * FROM channel c WHERE c.blocked ORDER BY c.name ASC")
    fun getBlacklist(): Single<List<Channel>>

    @Query("SELECT * FROM channel c WHERE c.topic == :topic")
    fun findByTopic(topic: String): Single<Channel?>

    @Insert
    fun create(channel: Channel): Completable

    @Update
    fun update(channel: Channel): Completable

    @Delete
    fun delete(channel: Channel): Completable
}