package at.apf.talkywalky.dao

import androidx.room.*
import at.apf.talkywalky.model.Walker
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface WalkerDao {

    @Query("SELECT * FROM walker w ORDER BY w.lastAccess DESC")
    fun getAll(): Single<List<Walker>>

    @Query("SELECT * FROM walker w WHERE w.blocked ORDER BY w.name ASC")
    fun getBlacklist(): Single<List<Walker>>

    @Query("SELECT * FROM walker w WHERE w.pk == :pk")
    fun findByPk(pk: String): Single<Walker?>

    @Insert
    fun create(walker: Walker): Completable

    @Update
    fun update(walker: Walker): Completable

    @Delete
    fun delete(walker: Walker): Completable
}