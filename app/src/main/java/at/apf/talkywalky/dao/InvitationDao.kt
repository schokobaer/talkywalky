package at.apf.talkywalky.dao

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import at.apf.talkywalky.model.Invitation
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface InvitationDao {

    @Query("SELECT * FROM Invitation")
    fun getAll(): Single<List<Invitation>>

    @Insert
    fun create(invitation: Invitation): Completable

    @Delete
    fun delete(invitation: Invitation): Completable

    @Query("DELETE FROM Invitation")
    fun drop(): Completable
}