package at.apf.talkywalky.ui

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceActivity
import android.util.Log
import at.apf.talkywalky.R
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.receiver.MuteBroadcastReceiver
import at.apf.talkywalky.service.PreferenceService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SettingsActivity : PreferenceActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addPreferencesFromResource(R.xml.prefs)

        this.findPreference("mute").setOnPreferenceChangeListener { _, newValue ->
            val new = newValue as Boolean
            if (new) {
                val intent = Intent(this, MuteBroadcastReceiver::class.java)
                sendBroadcast(intent)
            }
            true
        }

        this.findPreference("blacklist").setOnPreferenceClickListener {
            startActivity(Intent(this, BlacklistActivity::class.java))
            true
        }

        this.findPreference("clearInvitations").setOnPreferenceClickListener {
            AlertDialog.Builder(this)
                .setTitle("Drop Invitations")
                .setMessage("Are you sure you want to clear all outstanding invitations?")
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }.setPositiveButton("Yes") { dialog, _ ->
                    AppDatabase.getDb(this).invitationDao().drop()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            dialog.dismiss()
                        }, { err ->
                            Log.e("APP-DB", err.message)
                            err.printStackTrace()
                        })
                }.show()
            true
        }

        this.findPreference("name").summary = PreferenceService.getName(this)
        this.findPreference("name").setOnPreferenceChangeListener { preference, newValue ->
            preference.summary = newValue.toString()
            return@setOnPreferenceChangeListener true
        }
    }

}