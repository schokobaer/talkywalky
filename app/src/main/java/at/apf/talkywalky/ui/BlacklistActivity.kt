package at.apf.talkywalky.ui

import android.app.AlertDialog
import android.app.ListActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ListView
import at.apf.talkywalky.R
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.model.Contact
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class BlacklistActivity : ListActivity() {

    private lateinit var adapter: ArrayAdapter<String>
    private var items: MutableList<String> = LinkedList()
    private var contacts: List<Contact> = LinkedList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //requestWindowFeature(Window.FEATURE_ACTION_BAR)
        setContentView(R.layout.activity_list)
        title = "Blacklist"

        adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        listAdapter = adapter
        updateList()
    }

    override fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
        super.onListItemClick(l, v, position, id)
        val c = contacts[position]
        AlertDialog.Builder(this)
            .setTitle("Unblock")
            .setMessage("Do you want to unblock " + c.name + "?")
            .setNegativeButton("Cancel", { dialog, _ ->
                dialog.cancel()
            })
            .setPositiveButton("Yes", { dialog, _ ->
                c.blocked = false
                AppDatabase.getDb(this).contactRepo().update(c)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        dialog.dismiss()
                        updateList()
                    }, { err ->
                        err.printStackTrace()
                    })
            }).show()
    }

    private fun updateList() {
        AppDatabase.getDb(this).contactRepo().getBlacklist()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                contacts = it
                items.clear()
                items.addAll(it.map { c -> c.name })
                adapter.notifyDataSetChanged()
            }, { err ->
                Log.e("APP-DB", err.message)
                err.printStackTrace()
            })
    }

}
