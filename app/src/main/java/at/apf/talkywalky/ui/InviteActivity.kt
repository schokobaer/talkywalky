package at.apf.talkywalky.ui

import android.app.Activity
import android.os.Bundle
import android.widget.Toast
import at.apf.talkywalky.R
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.dto.InviteResponse
import at.apf.talkywalky.dto.QrCodeInvite
import at.apf.talkywalky.model.Walker
import at.apf.talkywalky.service.CryptoService
import at.apf.talkywalky.service.PreferenceService
import at.apf.talkywalky.util.QRCodeHelper
import com.google.gson.Gson
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_qrcodegen.*
import java.security.SecureRandom
import java.util.*
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.model.Invitation
import at.apf.talkywalky.service.SubscriptionService


class InviteActivity : Activity() {

    private lateinit var invite: QrCodeInvite
    private var shared: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_qrcodegen)
        instance = this

        // TODO: DEBUG
        /*this.qrCodeImageView.setOnClickListener {
            val url = "https://talkywalky-apf.web.app/contact.htm?c=eyJmY20iOiJjYWUwMTQ3MTNlYmJmMGZmNWZjN2U5MDM0MzlhZTNkZDA5ZThlZjExLUZING9ZYzZsOHdhVWZQX0lYYVlwaV9YenpaTVlSaXVMZ0ZkZkNLOVJObkxrR2JzaWtKLXk1OEk5cTBkSzAzcDdpTlp2aVo4RWpNQ2JzVktfYzF6NFFnLTMwYWUyZGY0OTEyZjFiNDMxY2JiYjAyN2Q3YWQ2YTEzOTVjYTVkYjUiLCJuYW1lIjoiaGdoZyIsInNlY3JldCI6Inl0eUVyaUFlMnZZeVA1Ul9iZ01QQWF2bllFVWczaE5HejBya29yWlZ6S3dcdTAwM2QiLCJ0eXBlIjoiY2hhbm5lbCJ9"
            startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        }*/

        this.radioShare.setOnCheckedChangeListener { group, checkedId ->
            if (invite.type == QrCodeInvite.TYPE_WALKER) {
                invite.expire = getExpire()
                updateQrCode()
            }
        }

        val channel = intent.getSerializableExtra(EXTRA_CHANNEL)
        if (channel != null) {
            initChannel(channel as Channel)
        } else {
            initWalker()
        }
    }

    override fun onResume() {
        super.onResume()
        if (shared) {
            finish()
        }
    }

    override fun onStop() {
        super.onStop()
        instance = null
    }

    private fun initWalker() {
        invite = QrCodeInvite(
            CryptoService.instance.generateSecretRandom(32),
            PreferenceService.getFcm(this),
            PreferenceService.getName(this),
            PreferenceService.getPublicKey(this),
            CryptoService.instance.generateSecretRandom(64),
            expire = getExpire()
        )

        updateQrCode()

        this.btnShare.setOnClickListener {
            val expire = getExpire()
            invite.expire = expire

            AppDatabase.getDb(this).invitationDao().create(
                Invitation(invite.secret, invite.challenge!!, expire)
            ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val uri = generateUri()

                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "text/plain"
                    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)

                    share.putExtra(Intent.EXTRA_SUBJECT, "Talky Walky Invitation")
                    share.putExtra(Intent.EXTRA_TEXT, uri)

                    shared = true
                    startActivityForResult(Intent.createChooser(share, "Share Invitation"), 1234)
                }, {err ->
                    Log.e("APP-DB", err.message)
                    err.printStackTrace()
                })
        }
    }

    private fun initChannel(channel: Channel) {
        invite = QrCodeInvite(
            channel.secret,
            channel.topic,
            channel.name,
            type = QrCodeInvite.TYPE_CHANNEL
        )

        updateQrCode()

        this.radioShare.visibility = View.GONE

        this.btnShare.setOnClickListener {
            val uri = generateUri()

            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET)

            share.putExtra(Intent.EXTRA_SUBJECT, "Talky Walky Group Invitation")
            share.putExtra(Intent.EXTRA_TEXT, uri)

            shared = true
            startActivityForResult(Intent.createChooser(share, "Share Invitation"), 1234)
        }
    }

    fun generateUri(): String {
        return "https://talkywalky-apf.web.app/contact.htm?c=" + invite.serialize()
    }

    fun handleHandshake(response: InviteResponse) {
        val challenge = CryptoService.instance.decrypt(response.challenge, invite.secret)
        val walker = Walker(response.fcm, response.pk, response.name, invite.secret)
        if (invite.challenge.equals(challenge)) {
            SubscriptionService.subscribe(SubscriptionService.generateRefreshTopicName(walker.pk))
            AppDatabase.getDb(this).contactRepo().create(walker)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ({
                    Toast.makeText(this, "Added " + response.name, Toast.LENGTH_SHORT).show()
                    this.finish()
                }, { _ ->
                    AppDatabase.getDb(this).contactRepo().update(walker)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            Toast.makeText(this, "Updated " + response.name, Toast.LENGTH_SHORT).show()
                            this.finish()
                        }, { err ->
                            err.printStackTrace()
                            this.finish()
                        })
                })
        }
    }

    private fun getExpire(): Long {
        var expire = Calendar.getInstance(Locale.getDefault())
        when (this.radioShare.checkedRadioButtonId) {
            R.id.radioHour -> expire.add(Calendar.HOUR, 1)
            R.id.radioDay -> expire.add(Calendar.HOUR, 24)
            R.id.radioWeek -> expire.add(Calendar.HOUR, 24 * 7)
        }
        return expire.time.time
    }

    private fun updateQrCode() {
        val bitmap = QRCodeHelper.newInstance(this)
            .setContent(generateUri())
            .setErrorCorrectionLevel(ErrorCorrectionLevel.L)
            .setMargin(2)
            .qrcOde
        qrCodeImageView.setImageBitmap(bitmap)
    }

    companion object {
        val EXTRA_CHANNEL = "channel"

        var instance: InviteActivity? = null
    }

}