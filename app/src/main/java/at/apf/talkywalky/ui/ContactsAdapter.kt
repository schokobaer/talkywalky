package at.apf.talkywalky.ui

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import at.apf.talkywalky.R
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.dto.RobotTalky
import at.apf.talkywalky.dto.TalkyEnvelope
import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.model.Walker
import at.apf.talkywalky.rest.RestClient
import at.apf.talkywalky.service.CryptoService
import at.apf.talkywalky.service.PreferenceService
import at.apf.talkywalky.service.RecorderService
import at.apf.talkywalky.service.SubscriptionService
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.ArrayList


class ContactsAdapter : RecyclerView.Adapter<ContactsAdapter.ViewHolder> {

    private val contactsList: ArrayList<Contact> = ArrayList()
    private val ctx: Context

    constructor(ctx: Context) {
        this.ctx = ctx
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_contact, parent, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return contactsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val c = contactsList[position]
        val recorder = RecorderService(c, ctx)
        holder.name.text = c.name
        holder.avatar.setImageDrawable(ctx.getDrawable(Contact.decide(c, R.drawable.walky2, R.drawable.channel2)))
        val textColor = if (c.silent) R.color.lightgrey else R.color.grey
            holder.name.setTextColor(
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) ContextCompat.getColor(ctx, textColor)
                else ctx.resources.getColor(textColor))
        val start: () -> Unit = {
            if (recorder.start()) {
                holder.name.text = "RECORDING"
                holder.item.setBackgroundColor(ctx.resources.getColor(R.color.colorAccent))
            }
        }
        var holding = AtomicInteger(0)
        val stop: () -> Unit = {
            holding.incrementAndGet()
            if (recorder.stop()) {
                holder.name.text = c.name
                holder.item.setBackgroundColor(ctx.resources.getColor(R.color.white))
                val subscription = if (c is Walker) AppDatabase.getDb(ctx).walkerDao().findByPk(c.pk)
                else if (c is Channel) AppDatabase.getDb(ctx).channelDao().findByTopic(c.topic)
                else throw ClassNotFoundException()
                subscription.subscribeOn(Schedulers.io())
                    .subscribe({
                        if (it != null) {
                            it.lastAccess = System.currentTimeMillis()
                            AppDatabase.getDb(ctx).contactRepo().update(it)
                                .subscribeOn(Schedulers.io())
                                .subscribe({
                                    Log.e("APP-DB", "Updated last access")
                                }, { err ->
                                    Log.e("APP-DB", err.message)
                                    err.printStackTrace()
                                })
                        }
                    }, { err ->
                        Log.e("APP-DB", err.message)
                        err.printStackTrace()
                    })
            }

        }
        holder.record.setOnTouchListener { _, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    val startValue = holding.incrementAndGet()
                    Handler().postDelayed({
                        if (holding.get() == startValue) {
                            start()
                        }
                    }, 100)
                }
                MotionEvent.ACTION_UP -> stop()
                MotionEvent.ACTION_CANCEL -> stop()
            }
            false
        }
        /*holder.record.isLongClickable = true
        holder.record.setOnLongClickListener {
            Log.i("LONG-CLICK","LONG-CLICK")
            start()
            true
        }*/
        var firstClick = Date()
        holder.record.setOnClickListener {
            val clickTime = Date().time - firstClick.time
            if (clickTime > 30 && clickTime < 500) {
                Log.i("DOUBLE-CLICK", "DOUBLE-CLICK")
                if (!recorder.isRecording()) {
                    start()
                }
            } else {
                firstClick = Date()
            }
        }

        holder.robot.setOnClickListener {
            val builder = AlertDialog.Builder(ctx)
            builder.setTitle("Send Robot Voice")
            builder.setView(R.layout.dialog_prompt)
            var input: EditText? = null
            builder.setPositiveButton(
                "Send"
            ) { dialog, which ->
                val content = input!!.text.toString()
                if (content.isEmpty()) {
                    Toast.makeText(ctx, "You can not send an empty robot talky", Toast.LENGTH_SHORT).show()
                    dialog.dismiss()
                    return@setPositiveButton
                }
                val crypto = CryptoService.instance
                Completable.create {
                    val encryptedText = crypto.encrypt(content, c.secret)
                    val receiver = Contact.fcmOrTopic(c)
                    val fan = Contact.decide(c, TalkyEnvelope.FAN_SINGLE, TalkyEnvelope.FAN_CHANNEL)
                    val signature = Contact.decide(c,
                        crypto.sign(encryptedText.toByteArray()),
                        crypto.encrypt(crypto.sha1(encryptedText.toByteArray()), c.secret))

                    val talky = RobotTalky(
                        PreferenceService.getPublicKey(ctx),
                        receiver,
                        encryptedText,
                        Locale.getDefault().language,
                        fan,
                        signature
                    )
                    RestClient.instance.postRobotTalky(talky)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            Toast.makeText(ctx, "Sent talky", Toast.LENGTH_SHORT).show()
                        }, { err -> err.printStackTrace()})
                }
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({

                    }, { err -> err.printStackTrace()})
                dialog.dismiss()
            }
            builder.setNegativeButton(
                "Cancel"
            ) { dialog, which -> dialog.cancel() }
            val dialog = builder.show()
            input = dialog.findViewById(R.id.input)
            input.maxLines = 3
            input.inputType = InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE
            input.hint = "\nMessage\n"
        }

        holder.item.setOnClickListener {
            ContactDialog(ctx, c)
        }
    }

    fun updateData(data: List<Contact>) {
        this.contactsList.clear()
        this.contactsList.addAll(data)
        this.notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val item = view as FrameLayout
        val avatar: ImageView = view.findViewById(R.id.avatar)
        val name: TextView = view.findViewById(R.id.lblName)
        val record: FloatingActionButton = view.findViewById(R.id.btnRecord)
        val robot: FloatingActionButton = view.findViewById(R.id.btnRobot)

    }
}