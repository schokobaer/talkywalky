package at.apf.talkywalky.ui

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import at.apf.talkywalky.MainActivity
import at.apf.talkywalky.R
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.model.Walker
import at.apf.talkywalky.service.SubscriptionService
//import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

class ContactDialog {

    private val ctx: Context
    private lateinit var contact: Contact
    private lateinit var dialog: AlertDialog

    constructor(ctx: Context, contact: Contact) {
        this.ctx = ctx
        val subscription = if (contact is Walker)
            AppDatabase.getDb(ctx).walkerDao().findByPk(contact.pk)
        else if (contact is Channel)
            AppDatabase.getDb(ctx).channelDao().findByTopic(contact.topic)
        else throw ClassNotFoundException()
        subscription.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it != null) {
                    this.contact = it
                    buildDialog()
                } else {
                    Toast.makeText(ctx, "Could not find walkerDao", Toast.LENGTH_SHORT).show()
                }
            }, { err ->
                Log.e("APP-DB", err.message)
                err.printStackTrace()
            })
    }

    fun buildDialog() {
        val builder = AlertDialog.Builder(ctx)
            .setTitle(contact.name)
            .setView(R.layout.dialog_contact)
            .setNegativeButton("Close") { _, _ ->
                hide()
            }

        dialog = builder.show()

        dialog.findViewById<TextView>(R.id.lblLastAccess).text = getLastAccessText()

        val btnDrop = dialog.findViewById<ImageButton>(R.id.btnDrop)
        btnDrop.setImageDrawable(ctx.getDrawable(R.drawable.trash))
        btnDrop.setOnClickListener {
            AlertDialog.Builder(ctx)
                .setTitle("Drop")
                .setMessage("Are you sure you want to delete " + contact.name + "?")
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }.setPositiveButton("Yes") { dialog, _ ->
                    AppDatabase.getDb(ctx).contactRepo().delete(contact)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (contact is Channel) {
                                SubscriptionService.unsubscribe((contact as Channel).topic)
                            } else if (contact is Walker){
                                SubscriptionService.unsubscribe(SubscriptionService.generateRefreshTopicName((contact as Walker).pk))
                            }
                            dialog.dismiss()
                            hide()
                        }, { err ->
                            Log.e("APP-DB", err.message)
                            err.printStackTrace()
                        })
                }.show()
        }

        val btnBlock = dialog.findViewById<ImageButton>(R.id.btnBlock)
        btnBlock.setImageDrawable(ctx.getDrawable(R.drawable.deny))
        btnBlock.setOnClickListener {
            AlertDialog.Builder(ctx)
                .setTitle("Block")
                .setMessage("Are you sure you want to block " + contact.name + "?")
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }.setPositiveButton("Yes") { dialog, _ ->
                    contact.blocked = true
                    updateContact()
                    dialog.dismiss()
                    hide()
                }.show()
        }

        setSilentImage()
        dialog.findViewById<ImageButton>(R.id.btnSilent).setOnClickListener {
            contact.silent = !contact.silent
            setSilentImage()
            updateContact()
        }

        val btnQrcode = dialog.findViewById<ImageButton>(R.id.btnQrcode)
        btnQrcode.setImageDrawable(ctx.getDrawable(R.drawable.qrcode))
        btnQrcode.visibility = if (contact is Channel) View.VISIBLE else View.GONE
        btnQrcode.setOnClickListener {
            val intent = Intent(ctx, InviteActivity::class.java)
            intent.putExtra(InviteActivity.EXTRA_CHANNEL, contact as Channel)
            ctx.startActivity(intent)
        }
    }

    private fun setSilentImage() {
        val silentResId = if (contact.silent) R.drawable.mute
            else R.drawable.sound
        dialog.findViewById<ImageButton>(R.id.btnSilent).setImageDrawable(ctx.getDrawable(silentResId))
    }

    private fun updateContact() {
        AppDatabase.getDb(ctx).contactRepo().update(contact).subscribeOn(Schedulers.io())
            .subscribe({
                Log.i("APP-DB", "Updated walkerDao")
            }, { err ->
                Log.e("APP-DB", err.message)
                err.printStackTrace()
            })
    }

    private fun getLastAccessText(): String {
        val date = Date(contact.lastAccess)
        val milies = Date().time - date.time

        if (milies / (1000 * 60) < 1) {
            return (milies / 1000).toString() + " Seconds ago"
        } else if (milies / (1000 * 60 * 60) < 1) {
            return (milies / (1000 * 60)).toString() + " Minutes ago"
        } else if (milies / (1000 * 60 * 60 * 24) < 1) {
            return date.hours.toString() + ":" + (if (date.minutes >= 10) date.minutes else "0" + date.minutes)
        }

        return date.day.toString() + "." + date.month + "."
    }

    fun hide() {
        if (dialog != null) {
            dialog!!.hide()
        }
        if (MainActivity.instace != null) {
            MainActivity.instace!!.updateContactList()
        }
    }

}