package at.apf.talkywalky.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.room.EmptyResultSetException
import at.apf.talkywalky.R
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.dto.InviteResponse
import at.apf.talkywalky.dto.InviteResponseEnvelope
import at.apf.talkywalky.dto.QrCodeInvite
import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.model.Walker
import at.apf.talkywalky.rest.RestClient
import at.apf.talkywalky.service.CryptoService
import at.apf.talkywalky.service.PreferenceService
import at.apf.talkywalky.service.SubscriptionService
//import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_urlreceiver.*
import java.lang.IllegalArgumentException
import java.util.*

class UrlReceiverActivity : Activity() {

    private lateinit var invite: QrCodeInvite

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_urlreceiver)
        this.logo.setImageDrawable(getDrawable(R.drawable.favicon))

        Log.i("URL-REC", "Received an url")

        if (intent == null || intent.data == null) {
            setResult(RESULT_CANCELED)
            finish()
            return
        }

        val uri = intent.data!!

        Log.i("URL-REC", "SCHEME: " + uri.host)
        Log.i("URL-REC", "HOST: " + uri.host)
        Log.i("URL-REC", "PATH: " + uri.path)

        invite = QrCodeInvite.deserialize(uri.getQueryParameter("c"))

        when {
            invite.type == QrCodeInvite.TYPE_WALKER -> handleWalker(invite)
            invite.type == QrCodeInvite.TYPE_CHANNEL -> handleChannel(invite)
            else -> throw IllegalArgumentException()
        }

        this.btnCancel.setOnClickListener {
            setResult(RESULT_CANCELED)
            finish()
        }

        this.btnAdd.setOnClickListener {
            val contact = if (invite.type == QrCodeInvite.TYPE_CHANNEL) Channel(invite.fcm, invite.name, invite.secret)
            else Walker(invite.fcm, invite.pk!!, invite.name, invite.secret)
            AppDatabase.getDb(this).contactRepo().create(contact)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (invite.type == QrCodeInvite.TYPE_CHANNEL) {
                        SubscriptionService.subscribe(invite.fcm)
                        Toast.makeText(this, "Added " + invite.name, Toast.LENGTH_SHORT).show()
                        Log.i("INVITERESPONSE", "Success")
                        setResult(RESULT_OK)
                        finish()
                    } else {
                        SubscriptionService.subscribe(SubscriptionService.generateRefreshTopicName((contact as Walker).pk))
                        sendInviteResponse()
                    }
                }, {
                    AppDatabase.getDb(this).contactRepo().update(contact)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            if (invite.type == QrCodeInvite.TYPE_CHANNEL) {
                                SubscriptionService.subscribe(invite.fcm)
                                Toast.makeText(this, "Updated " + invite.name, Toast.LENGTH_SHORT).show()
                                Log.i("INVITERESPONSE", "Success")
                                setResult(RESULT_OK)
                                finish()
                            } else {
                                SubscriptionService.subscribe(SubscriptionService.generateRefreshTopicName((contact as Walker).pk))
                                sendInviteResponse()
                            }
                        }, { err ->
                            Log.e("APP-DB", err.message)
                            err.printStackTrace()
                            setResult(RESULT_CANCELED)
                            finish()
                        })
                })
        }
    }

    private fun handleWalker(invite: QrCodeInvite) {
        val dao = AppDatabase.getDb(this).walkerDao()
        val now = Date()

        val expire = Date(invite.expire ?: Date().time)

        if (expire.before(now)) {
            this.text.text = "Invitation expired"
            this.btnAdd.visibility = View.GONE
        } else if (invite.pk == PreferenceService.getPublicKey(this)) {
            this.text.text = "You can not add yourself"
            this.btnAdd.visibility = View.GONE
        } else {
            dao.findByPk(invite.pk!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ contact ->
                    this.text.text = "Do you want to override ${invite.name}?"
                    this.btnAdd.text = "Override"
                }, { err ->
                    if (err is EmptyResultSetException) {
                        this.text.text = "Do you want to add ${invite.name}?"
                    } else {
                        Log.e("APP-DB", err.message)
                        err.printStackTrace()
                    }
                })
        }
    }

    private fun handleChannel(invite: QrCodeInvite) {

        AppDatabase.getDb(this).channelDao().findByTopic(invite.fcm)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ contact ->
                this.text.text = "Do you want to override the channel ${invite.name}?"
                this.btnAdd.text = "Override"
            }, { err ->
                if (err is EmptyResultSetException) {
                    this.text.text = "Do you want to add the channel ${invite.name}?"
                } else {
                    Log.e("APP-DB", err.message)
                    err.printStackTrace()
                }
            })
    }

    fun sendInviteResponse() {
        val response = InviteResponse(
            PreferenceService.getFcm(this),
            PreferenceService.getPublicKey(this),
            PreferenceService.getName(this),
            CryptoService.instance.encrypt(invite.challenge!!, invite.secret)
        )
        val responseEnvelope = InviteResponseEnvelope(invite.fcm, response, "")
        RestClient.instance.respondInvite(responseEnvelope)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Toast.makeText(this, "Added " + invite.name, Toast.LENGTH_SHORT).show()
                Log.i("INVITERESPONSE", "Success")
                setResult(RESULT_OK)
                finish()
            }, { err ->
                Log.e("INVITERESPONSE", err.message)
                Toast.makeText(this, "Error adding contact", Toast.LENGTH_SHORT).show()
                err.printStackTrace()
                setResult(RESULT_CANCELED)
                finish()
            })
    }
}