package at.apf.talkywalky.ui

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import at.apf.talkywalky.R
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import me.dm7.barcodescanner.zxing.ZXingScannerView
import kotlinx.android.synthetic.main.activity_scanner.*

class ScannerActivity: Activity(), ZXingScannerView.ResultHandler {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)

        qrCodeScanner.setFormats(listOf(BarcodeFormat.QR_CODE))
        qrCodeScanner.setAutoFocus(true)
        qrCodeScanner.setLaserColor(R.color.colorAccent)
        qrCodeScanner.setMaskColor(R.color.colorAccent)
        //if (Build.MANUFACTURER.equals(HUAWEI, ignoreCase = true))
        //qrCodeScanner.setAspectTolerance(0.5f)
    }

    override fun onResume() {
        super.onResume()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA),
                    1234)
                return
            }
        }
        qrCodeScanner.startCamera()
        qrCodeScanner.setResultHandler(this)
    }

    override fun handleResult(result: Result?) {
        val pattern = "https://talkywalky-apf.web.app/contact.htm?c="
        if (result != null && result.text.startsWith(pattern)) {
            val intent = Intent(this, UrlReceiverActivity::class.java)
            intent.data = Uri.parse(result.text)
            startActivityForResult(intent, 1234)
        } else {
            Toast.makeText(this, "Not a valid QR Code for Talky Walky", Toast.LENGTH_SHORT).show()
            Log.i("QRCODE", "Not Valid for TalkyWalky: " + result?.text)
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1234) {
            finish()
        }
    }

    override fun onPause() {
        super.onPause()
        qrCodeScanner.stopCamera()
    }
}