package at.apf.talkywalky.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import at.apf.talkywalky.App
import at.apf.talkywalky.MainActivity
import at.apf.talkywalky.R
import at.apf.talkywalky.service.CryptoService
import at.apf.talkywalky.service.PreferenceService
import io.reactivex.Completable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_register.*

class SetupActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (App.isInitialized()) {
            showMain()
            return
        }

        setContentView(R.layout.activity_register)

        this.logo.setImageDrawable(getDrawable(R.drawable.favicon))

        this.btnRegister.setOnClickListener {
            val name = this.tbxName.text.toString()
            if (name.length < 2 && name.length < 15) {
                return@setOnClickListener
            }
            CryptoService.instance.generateKeypair(this)
            PreferenceService.saveName(this, name)

            showMain()
        }
    }

    private fun showMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}