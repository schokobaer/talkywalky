package at.apf.talkywalky.rest

import at.apf.talkywalky.dto.FcmRefreshRequest
import at.apf.talkywalky.dto.InviteResponseEnvelope
import at.apf.talkywalky.dto.RobotTalky
import io.reactivex.Completable
import io.reactivex.Observable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST

interface RestClient {

    @POST("messaging/invite")
    fun respondInvite(@Body inviteResponseEnvelope: InviteResponseEnvelope): Completable

    @POST("messaging/refresh")
    fun refreshFcm(@Body fcmRefreshRequest: FcmRefreshRequest): Completable

    @POST("messaging/robot")
    fun postRobotTalky(@Body robotTalky: RobotTalky): Completable

    companion object {
        val instance: RestClient = Retrofit.Builder()
            .baseUrl("https://us-central1-talkywalky-apf.cloudfunctions.net/api/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(OkHttpClient.Builder().build())
            .build()
            .create(RestClient::class.java)
    }
}