package at.apf.talkywalky

class Const {
    companion object {
        val STORAGE_META_TALKY_ENVELOPE = "envelope" // JSON-TalkyEnvelope
        val FCM_TALKY_KEY = "talky" // defines the talkyEnvelope
        val FCM_ACTION_KEY = "action" // defines the action (invite | talky)
        val FCM_RESPONSE_KEY = "response" // defines the invite response
        val FCM_REFRESH_KEY = "refresh" // defines the refresh task
    }
}