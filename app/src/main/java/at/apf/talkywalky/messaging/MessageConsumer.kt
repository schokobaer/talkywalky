package at.apf.talkywalky.messaging

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.PowerManager
import at.apf.talkywalky.Const
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.dto.FcmRefreshTask
import at.apf.talkywalky.dto.InviteResponse
import at.apf.talkywalky.dto.RobotTalky
import at.apf.talkywalky.dto.TalkyEnvelope
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.model.Walker
import at.apf.talkywalky.service.*
import at.apf.talkywalky.ui.InviteActivity
import at.apf.talkywalky.util.Logger
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

class MessageConsumer(val ctx: Context) {

    private val logger = Logger(ctx, MessageConsumer::class.java)

    private val firebaseStore: StorageReference = FirebaseStorage.getInstance().reference
    private val incrementer: AtomicInteger = AtomicInteger(0)

    fun consume(bundle: Bundle?, onComplete: CompleteListener) {
        logger.info("Received a message")
        turnOnScreen()

        val data = bundle!!

        if (data.isEmpty || !data.containsKey(Const.FCM_ACTION_KEY)) {
            return
        }

        when (data[Const.FCM_ACTION_KEY]) {
            MessageType.INVITE -> invite(data.getString(Const.FCM_RESPONSE_KEY), onComplete)
            MessageType.TALKY -> talky(data.getString(Const.FCM_TALKY_KEY), onComplete)
            MessageType.ROBOT_TALKY -> robotTalky(data.getString(Const.FCM_TALKY_KEY), onComplete)
            MessageType.REFRESH -> refresh(data.getString(Const.FCM_REFRESH_KEY), onComplete)
        }
    }

    fun invite(data: String, onComplete: CompleteListener) {
        logger.info("Got a invite")
        logger.info(data)

        val inviteResponse = Gson().fromJson<InviteResponse>(data, InviteResponse::class.java)
        val dao = AppDatabase.getDb(ctx).invitationDao()
        dao.getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ list ->
                val found = list
                    .filter { i -> inviteResponse.challenge == CryptoService.instance.encrypt(i.challenge, i.secret) }
                    .firstOrNull()
                if (found != null) {
                    val expireDate = Date(found.expireDate)
                    if (expireDate.after(Date())) {
                        val contact = Walker(
                            inviteResponse.fcm,
                            inviteResponse.pk,
                            inviteResponse.name,
                            found.secret
                        )
                        AppDatabase.getDb(ctx)
                            .walkerDao()
                            .create(contact)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe ({
                                NotificationService.getInstance(ctx).contactAdded(contact)
                                onComplete(true)
                            }, { err ->
                                logger.warn("Already in contacts: " + contact.name)
                                onComplete(true)
                            })
                    }
                    dao.delete(found).subscribeOn(Schedulers.io()).subscribe()
                } else {
                    onComplete(true)
                    InviteActivity.instance?.handleHandshake(inviteResponse)
                }
            }, { it.printStackTrace(); onComplete(false) })
    }

    fun refresh(data: String, onComplete: CompleteListener) {
        logger.info("Got a refresh")
        logger.info(data)
        val fcmRefreshTask = Gson().fromJson<FcmRefreshTask>(data, FcmRefreshTask::class.java)
        val crypto = CryptoService.instance
        if (!crypto.verify(crypto.deserializePublicKey(fcmRefreshTask.pk), fcmRefreshTask.signature, fcmRefreshTask.fcm)) {
            logger.warn("Verification failed")
            onComplete(false)
            return
        }
        AppDatabase.getDb(ctx).walkerDao().findByPk(fcmRefreshTask.pk)
            .subscribeOn(Schedulers.io())
            .subscribe({ contact ->
                if (contact != null) {
                    contact.fcm = fcmRefreshTask.fcm
                    AppDatabase.getDb(ctx).walkerDao().update(contact)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            logger.info("Successfully refreshed fcm from " + fcmRefreshTask.pk)
                            onComplete(true)
                        }, { err ->
                            logger.error(err.message ?: "", err)
                            err.printStackTrace()
                            onComplete(false)
                        })
                }
            }, { err ->
                logger.error(err.message ?: "", err)
                err.printStackTrace()
                onComplete(false)
            })
    }

    fun talky(data: String, onComplete: CompleteListener) {
        logger.info("Got a talky")
        if (PreferenceService.isMute(ctx)) {
            onComplete(false)
            return
        }
        val talkyEnvelope = Gson().fromJson<TalkyEnvelope>(data, TalkyEnvelope::class.java)
        if (talkyEnvelope.fan == TalkyEnvelope.FAN_SINGLE) {
            AppDatabase.getDb(ctx).walkerDao().findByPk(talkyEnvelope.senderPk)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it != null) {
                        processTalky(it, talkyEnvelope, onComplete)
                    }
                }, { err ->
                    logger.error(err.message ?: "", err)
                    err.printStackTrace()
                    onComplete(false)
                })
        } else {
            if (talkyEnvelope.senderPk != PreferenceService.getPublicKey(ctx) && talkyEnvelope.fcm != null) {
                AppDatabase.getDb(ctx).channelDao().findByTopic(talkyEnvelope.fcm)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it != null) {
                            processTalky(it, talkyEnvelope, onComplete)
                        }
                    }, { err ->
                        logger.error(err.message ?: "", err)
                        err.printStackTrace()
                        onComplete(false)
                    })
            }
        }
    }

    fun robotTalky(data: String, onComplete: CompleteListener) {
        logger.info("Got a robot talky")
        if (PreferenceService.isMute(ctx)) {
            onComplete(false)
            return
        }
        val talkyEnvelope = Gson().fromJson<RobotTalky>(data, RobotTalky::class.java)
        if (talkyEnvelope.fan == TalkyEnvelope.FAN_SINGLE) {
            AppDatabase.getDb(ctx).walkerDao().findByPk(talkyEnvelope.senderPk)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it != null) {
                        processRobotTalky(it, talkyEnvelope, onComplete)
                    }
                }, { err ->
                    logger.error(err.message ?: "", err)
                    err.printStackTrace()
                    onComplete(false)
                })
        } else {
            if (talkyEnvelope.senderPk != PreferenceService.getPublicKey(ctx) && talkyEnvelope.fcm != null) {
                AppDatabase.getDb(ctx).channelDao().findByTopic(talkyEnvelope.fcm)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it != null) {
                            processRobotTalky(it, talkyEnvelope, onComplete)
                        }
                    }, { err ->
                        logger.error(err.message ?: "", err)
                        err.printStackTrace()
                        onComplete(false)
                    })
            }
        }
    }

    fun processTalky(contact: Contact, talkyEnvelope: TalkyEnvelope, onComplete: CompleteListener) {
        if (contact.blocked || contact.silent) {
            onComplete(false)
            return
        }

        val fileRef = firebaseStore.child(talkyEnvelope.filePath!!)
        val recordFile = File.createTempFile(contact.name, incrementer.getAndIncrement().toString(), ctx.cacheDir)
        fileRef.getFile(recordFile)
            .addOnSuccessListener {
                if (verifyTalky(recordFile, talkyEnvelope.signature, contact)) {
                    decrypt(recordFile, contact.secret)
                    updateLastAccess(contact)
                    MediaPlayerService.getInstance(ctx).enqueue(contact, recordFile, onComplete)
                } else {
                    logger.warn("Wrong signature on talky")
                    onComplete(false)
                }
            }
            .addOnFailureListener {err ->
                logger.error(err.message ?: "", err)
                onComplete(false)
            }
    }

    fun processRobotTalky(contact: Contact, talky: RobotTalky, onComplete: CompleteListener) {
        if (contact.blocked || contact.silent) {
            onComplete(false)
            return
        }

        val crypto = CryptoService.instance
        if (verifyTalky(talky.text, talky.signature, contact)) {
            updateLastAccess(contact)
            val decrText = crypto.decrypt(talky.text, contact.secret)
            TtsService.getInstance(ctx).synthesize(decrText, talky.locale, contact, onComplete)
        } else {
            logger.warn("Wrong signature on talky")
            onComplete(false)
        }

    }

    fun decrypt(file: File, key: String) {
        val decRaw = CryptoService.instance.decrypt(file.readBytes(), key)
        file.writeBytes(decRaw)
    }

    fun verifyTalky(file: File, signature: String, contact: Contact): Boolean {
        val crypto = CryptoService.instance
        if (contact is Walker) {
            val pk = crypto.deserializePublicKey(contact.pk)
            return crypto.verify(pk, signature, file.readBytes())
        } else {
            val decSha1 = crypto.encrypt(crypto.sha1(file.readBytes()), contact.secret)
            return decSha1 == signature
        }
    }

    fun verifyTalky(content: String, signature: String, contact: Contact): Boolean {
        val crypto = CryptoService.instance
        if (contact is Walker) {
            val pk = crypto.deserializePublicKey(contact.pk)
            return crypto.verify(pk, signature, content)
        } else {
            val decSha1 = crypto.encrypt(crypto.sha1(content), contact.secret)
            return decSha1 == signature
        }
    }

    fun updateLastAccess(contact: Contact) {
        contact.lastAccess = System.currentTimeMillis()
        AppDatabase.getDb(ctx).contactRepo().update(contact)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                logger.info("Updated lastAccess of " + contact.name)
            }
    }

    fun turnOnScreen() {
        // Turn on the screen for notification
        val powerManager = ctx.getSystemService(Context.POWER_SERVICE) as PowerManager
        val result =
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH && powerManager.isInteractive || Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT_WATCH && powerManager.isScreenOn

        if (!result) {
            val wl = powerManager.newWakeLock(
                PowerManager.FULL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or PowerManager.ON_AFTER_RELEASE,
                "myapp:mywakelocktag"
            )
            wl.acquire(10000)
            val wl_cpu = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "myapp:mywakelocktag")
            wl_cpu.acquire(10000)
        }
    }

    companion object {
        private var instance: MessageConsumer? = null
        fun getInstance(ctx: Context): MessageConsumer {
            if (instance == null) {
                instance = MessageConsumer(ctx)
            }
            return instance!!
        }
    }
}