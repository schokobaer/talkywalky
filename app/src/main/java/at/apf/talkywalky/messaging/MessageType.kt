package at.apf.talkywalky.messaging

class MessageType {
    companion object {
        val TALKY = "talky"
        val ROBOT_TALKY = "robot_talky"
        val INVITE = "invite"
        val REFRESH = "refresh"
    }
}

typealias CompleteListener = (Boolean) -> Unit