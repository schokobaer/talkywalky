
package at.apf.talkywalky.messaging

import android.app.job.JobInfo
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import at.apf.talkywalky.dto.*
import at.apf.talkywalky.rest.RestClient
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import android.os.Bundle
import android.os.PersistableBundle
import at.apf.talkywalky.App
import at.apf.talkywalky.service.*
import at.apf.talkywalky.util.Logger
import java.util.concurrent.Semaphore


class FirebaseCloudMessagingService : FirebaseMessagingService() {

    private val logger = Logger(this, FirebaseCloudMessagingService::class.java)

    override fun onMessageReceived(msg: RemoteMessage) {
        logger.info("Received a message")

        val extras = Bundle()
        for (key in msg.data.keys) {
            extras.putString(key, msg.data[key])
        }

        val lock = Semaphore(0)
        MessageConsumer.getInstance(this).consume(extras) { lock.release() }

        lock.acquire()
        logger.info("Finished work")

    }

    private fun executeAsJob(msg: RemoteMessage) {
        val scheduler = applicationContext.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler
        val componentName = ComponentName(this, MessageJob::class.java)
        val extras = PersistableBundle()
        var hash = 0L
        for (key in msg.data.keys) {
            extras.putString(key, msg.data[key])
            hash += msg.data[key].hashCode()
        }
        val jobInfo = JobInfo.Builder(hash.toInt(), componentName)
            .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
            .setExtras(extras)
            .build()
        scheduler.schedule(jobInfo)
    }


    override fun onNewToken(token: String) {
        logger.info("New Token: $token")
        PreferenceService.saveFcm(this, token)
        if (!App.isInitialized()) {
            return
        }
        RestClient.instance.refreshFcm(
            FcmRefreshRequest(
                token,
                PreferenceService.getPublicKey(this),
                CryptoService.instance.sign(token.toByteArray())
            )
        )
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                logger.info("Successfully sent fcm refresh request")
            }, { err ->
                logger.error(err.message ?: "", err)
                err.printStackTrace()
            })
    }

}