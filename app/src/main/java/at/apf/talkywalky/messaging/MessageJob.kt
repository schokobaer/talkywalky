package at.apf.talkywalky.messaging

import android.app.job.JobParameters
import android.app.job.JobService
import android.os.Bundle
import at.apf.talkywalky.util.Logger



class MessageJob : JobService() {

    private val logger = Logger(this, MessageJob::class.java)

    override fun onStartJob(params: JobParameters?): Boolean {
        logger.info("Starting new job")

        if (params == null) {
            return false
        }

        val extras = Bundle()
        for (key in params.extras.keySet()) {
            extras.putString(key, params.extras.getString(key))
        }

        MessageConsumer.getInstance(this).consume(extras) { jobFinished(params, false)}

        return true
    }

    override fun onStopJob(params: JobParameters?): Boolean {
        return false
    }
}