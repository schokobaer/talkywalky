package at.apf.talkywalky.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import at.apf.talkywalky.messaging.FirebaseCloudMessagingService
import at.apf.talkywalky.util.Logger

class BootBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("BOOT", "Booted. Starting FCM Service now ...")
        if (context != null) {
            Logger.log(context, "Booted. Starting FCM Service now ...")
        }
        context?.startService(Intent(context, FirebaseCloudMessagingService::class.java))
    }
}