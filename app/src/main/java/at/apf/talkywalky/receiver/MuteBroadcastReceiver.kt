package at.apf.talkywalky.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import at.apf.talkywalky.service.MediaPlayerService
import at.apf.talkywalky.service.NotificationService
import at.apf.talkywalky.service.PreferenceService

class MuteBroadcastReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        Log.i("MUTE-REC", "Received a mute request")
        if (context != null) {
            MediaPlayerService.getInstance(context).mute()
            if (!PreferenceService.isMute(context)) {
                PreferenceService.toggleMute(context)
            }
            NotificationService.getInstance(context).hide()
        }
    }
}