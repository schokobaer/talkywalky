package at.apf.talkywalky.service

import android.util.Log
import com.google.firebase.messaging.FirebaseMessaging

class SubscriptionService {
    companion object {

        fun subscribe(topic: String) {
            FirebaseMessaging.getInstance().subscribeToTopic(topic)
                .addOnSuccessListener { Log.i("SUBSCRIBE", "Success subscription to " + topic) }
                .addOnFailureListener { err -> Log.e("SUBSCRIBE", err.message) }
        }

        fun unsubscribe(topic: String) {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(topic)
                .addOnSuccessListener { Log.i("UNSUBSCRIBE", "Success unsubscription from " + topic) }
                .addOnFailureListener { err -> Log.e("UNSUBSCRIBE", err.message) }
        }

        fun generateTopicName(walkerName: String, channelName: String): String {
            val cry = CryptoService.instance
            val topic = cry.sha1(walkerName) + "-" + cry.generateSecretRandom(64) + "-" + cry.sha1(channelName)
            return topic.replace("=", "")
        }

        fun generateRefreshTopicName(pk: String): String {
            return "walker." + pk.replace("=", "")
        }
    }
}