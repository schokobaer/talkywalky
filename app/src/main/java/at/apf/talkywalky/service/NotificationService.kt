package at.apf.talkywalky.service

import android.app.*
import android.content.Context
import androidx.core.app.NotificationCompat
import at.apf.talkywalky.R
import at.apf.talkywalky.model.Walker
import java.util.concurrent.atomic.AtomicBoolean
import android.content.Intent
import android.os.Build.VERSION_CODES.O
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.widget.RemoteViews
import at.apf.talkywalky.MainActivity
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.receiver.MuteBroadcastReceiver
import java.util.concurrent.atomic.AtomicInteger


class NotificationService {

    private val ctx: Context
    private val receivedIncoming: AtomicInteger = AtomicInteger(0)

    constructor(ctx: Context) {
        this.ctx = ctx
    }

    fun staticNotification(): Notification {
        val nManager = (ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)

        val builder = NotificationCompat.Builder(ctx, "mychannel_003")
            //.setContentTitle("")
            .setSmallIcon(R.drawable.favicon)
            .setOngoing(true)
            .setAutoCancel(false)

        val intent = Intent(ctx, MainActivity::class.java)
        intent.action = Intent.ACTION_MAIN
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val stackBuilder = TaskStackBuilder.create(ctx)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(intent)
        val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)


        if (Build.VERSION.SDK_INT >= O) {
            val channel = NotificationChannel(
                "mychannel_003",
                "TalkyWalky",
                NotificationManager.IMPORTANCE_HIGH
            )
            nManager.createNotificationChannel(channel)
            builder.setChannelId("mychannel_003")
        }

        return builder.build()
    }

    fun contactAdded(walker: Walker) {
        val nManager = (ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)

        val builder = NotificationCompat.Builder(ctx, "mychannel_002")
            .setContentTitle("New Walker")
            .setSmallIcon(R.drawable.favicon)
            .setOngoing(false)
            .setAutoCancel(true)

        val intent = Intent(ctx, MainActivity::class.java)
        intent.action = Intent.ACTION_MAIN
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val stackBuilder = TaskStackBuilder.create(ctx)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(intent)
        val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)


        builder.setContentText("Added " + walker.name)
            .setContentInfo("Added from " + walker.name)

        if (Build.VERSION.SDK_INT >= O) {
            val channel = NotificationChannel(
                "mychannel_002",
                "Contacts",
                NotificationManager.IMPORTANCE_HIGH
            )
            nManager.createNotificationChannel(channel)
            builder.setChannelId("mychannel_002")
        }

        nManager.notify(walker.hashCode(), builder.build())
    }

    fun hide() {
        (ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)
            .cancel(NOTIFICATION_ID)
    }

    fun incoming(contact: Contact) {
        displayNotification(contact)
        receivedIncoming.incrementAndGet()
    }

    fun resetDelayed() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (receivedIncoming.decrementAndGet() == 0) {
                reset()
            }
        }, 10 * 1000)
    }

    fun reset() {
        hide()
    }

    private fun displayNotification(contact: Contact) {
        val nManager = (ctx.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager)

        val builder = NotificationCompat.Builder(ctx, NOTIFICATION_CHANNEL)
            .setContentTitle("Talky Walky")
            .setSmallIcon(R.drawable.favicon)
            .setOngoing(false)
            .setAutoCancel(true)

        val intent = Intent(ctx, MainActivity::class.java)
        intent.action = Intent.ACTION_MAIN
        intent.addCategory(Intent.CATEGORY_LAUNCHER)
        val stackBuilder = TaskStackBuilder.create(ctx)
        stackBuilder.addParentStack(MainActivity::class.java)
        stackBuilder.addNextIntent(intent)
        val pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(pendingIntent)

        val view = RemoteViews(ctx.packageName, R.layout.notification_talky)
        view.setTextViewText(R.id.lblSender, "Talky from " + (contact?.name))
        view.setImageViewResource(R.id.favicon, R.drawable.favicon)
        view.setImageViewResource(R.id.btnSettings, R.drawable.mute)

        val muteIntent = Intent(ctx, MuteBroadcastReceiver::class.java)
        val mutePendingIntent = PendingIntent.getBroadcast(ctx, 1234, muteIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        view.setOnClickPendingIntent(R.id.btnSettings, mutePendingIntent)

        builder.setContentText("Talky from " + contact.name)
            .setContentInfo("Talky from " + contact.name)
        builder.setContent(view)



        if (Build.VERSION.SDK_INT >= O) {
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.enableVibration(false)
            nManager.createNotificationChannel(channel)
            builder.setChannelId(NOTIFICATION_CHANNEL)
        }

        nManager.notify(NOTIFICATION_ID, builder.build())
    }

    companion object {
        private val NOTIFICATION_ID = 99
        private val NOTIFICATION_CHANNEL = "mychannel_001"
        private var instance: NotificationService? = null

        fun getInstance(ctx: Context): NotificationService {
            if (instance == null) {
                instance = NotificationService(ctx)
            }
            return instance!!
        }
    }
}