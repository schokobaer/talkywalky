package at.apf.talkywalky.service

import android.content.Context
import android.os.Build
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import java.io.File
import java.security.spec.ECGenParameterSpec
import com.google.android.gms.common.util.Base64Utils
import java.security.*
import android.security.KeyChain.getPrivateKey
import android.security.KeyPairGeneratorSpec
import android.util.Base64
import androidx.annotation.RequiresApi
import java.io.UnsupportedEncodingException
import java.lang.Exception
import java.math.BigInteger
import java.security.spec.KeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.*
import javax.crypto.Cipher
import javax.crypto.spec.SecretKeySpec
import java.util.Base64.getEncoder
import java.util.Base64.getDecoder
import javax.security.auth.x500.X500Principal

class CryptoService {

    fun toBase64(data: ByteArray): String {
        return Base64.encodeToString(data, Base64.URL_SAFE or Base64.NO_WRAP)
    }

    fun toBase64(data: String): String {
        return toBase64(data.toByteArray())
    }

    fun fromBase64(base64: String): ByteArray {
        return Base64.decode(base64, Base64.URL_SAFE or Base64.NO_WRAP)
    }

    fun generateKeypair(ctx: Context) {
        val keyGen = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, ANDROIDKEYSTORE)

        //keyGen.initialize(ECGenParameterSpec("secp256r1"), SecureRandom())
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            initSdk23(keyGen) else initSdk21(keyGen, ctx)

        val kp = keyGen.genKeyPair()
        val pk = toBase64(kp.public.encoded)
        PreferenceService.savePublicKey(ctx, pk)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun initSdk23(keyGen: KeyPairGenerator) {
        keyGen.initialize(KeyGenParameterSpec.Builder(
            ALIAS,
            KeyProperties.PURPOSE_SIGN or KeyProperties.PURPOSE_VERIFY)
            .setDigests(KeyProperties.DIGEST_SHA256, KeyProperties.DIGEST_SHA512)
            .setKeySize(256)
            .build(), SecureRandom())
    }

    private fun initSdk21(keyGen: KeyPairGenerator, ctx: Context) {
        val start = Calendar.getInstance()
        val end = Calendar.getInstance()
        end.add(Calendar.YEAR, 5)
        keyGen.initialize(KeyPairGeneratorSpec.Builder(ctx)
            .setAlias(ALIAS)
            .setKeySize(256)
            .setSubject(X500Principal("CN=Sample Name, O=Android Authority"))
            .setSerialNumber(BigInteger.ONE)
            .setStartDate(start.time)
            .setEndDate(end.time)
            .setKeyType(KeyProperties.KEY_ALGORITHM_EC)
            .build(), SecureRandom()
        )
    }

    fun holdsKeys(): Boolean {
        val keyStore = KeyStore.getInstance(ANDROIDKEYSTORE)
        keyStore.load(null)
        return keyStore.getEntry(ALIAS, null) != null
    }

    fun loadPrivateKey(): PrivateKey {
        val keyStore = KeyStore.getInstance(ANDROIDKEYSTORE)
        keyStore.load(null)
        val entry = keyStore.getEntry(ALIAS, null)
        return (entry as KeyStore.PrivateKeyEntry).privateKey
    }

    fun loadPublicKey(): PublicKey {
        val keyStore = KeyStore.getInstance(ANDROIDKEYSTORE)
        keyStore.load(null)
        val entry = keyStore.getEntry(ALIAS, null)
        return keyStore.getCertificate(ALIAS).publicKey
    }

    fun sha1(data: ByteArray): String {
        val md = MessageDigest.getInstance("SHA-1")
        val messageDigest = md.digest(data)
        val no = BigInteger(1, messageDigest)
        var hashtext = no.toString(16)
        while (hashtext.length < 32) {
            hashtext = "0$hashtext"
        }
        return hashtext
    }

    fun sha1(data: String): String {
        return sha1(data.toByteArray())
    }

    fun sign(data: ByteArray): String {
        val signature = Signature.getInstance("SHA256withECDSA")
        signature.initSign(loadPrivateKey())

        signature.update(data)

        return toBase64(signature.sign())
    }

    fun sign(privateKey: PrivateKey, message: String): String {
        val signature = Signature.getInstance("SHA256withECDSA")
        signature.initSign(privateKey)

        signature.update(message.toByteArray())

        return toBase64(signature.sign())
    }

    fun verify(publicKey: PublicKey, signed: String, message: String): Boolean {
        val signature = Signature.getInstance("SHA256withECDSA")
        signature.initVerify(publicKey)

        signature.update(message.toByteArray())

        return signature.verify(fromBase64(signed))
    }

    fun verify(publicKey: PublicKey, signed: String, data: ByteArray): Boolean {
        val signature = Signature.getInstance("SHA256withECDSA")
        signature.initVerify(publicKey)

        signature.update(data)

        return signature.verify(fromBase64(signed))
    }

    fun encrypt(data: ByteArray, key: String): ByteArray {
        val secretKey = createAesKey(key)
        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        return cipher.doFinal(data)
    }

    fun encrypt(data: String, key: String): String {
        val secretKey = createAesKey(key)
        val cipher = Cipher.getInstance("AES/ECB/PKCS5Padding")
        cipher.init(Cipher.ENCRYPT_MODE, secretKey)
        return toBase64(cipher.doFinal(data.toByteArray()))
    }

    fun decrypt(data: ByteArray, key: String): ByteArray {
        try {
            val secretKey = createAesKey(key)
            val cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING")
            cipher.init(Cipher.DECRYPT_MODE, secretKey)
            return cipher.doFinal(data)
        } catch (e: Exception) {
            return ByteArray(0)
        }
    }

    fun decrypt(data: String, key: String): String {
        try {
            val secretKey = createAesKey(key)
            val cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING")
            cipher.init(Cipher.DECRYPT_MODE, secretKey)
            return String(cipher.doFinal(fromBase64(data)))
        } catch (e: Exception) {
            return ""
        }
    }

    fun createAesKey(strKey: String): SecretKeySpec {
        var sha: MessageDigest = MessageDigest.getInstance(KeyProperties.DIGEST_SHA256)
        var key = strKey.toByteArray()
        key = sha.digest(key)
        key = Arrays.copyOf(key, 32)
        return SecretKeySpec(key, "AES")

    }

    fun deserializePublicKey(key: String): PublicKey {
        val byteKey = fromBase64(key)
        val X509publicKey = X509EncodedKeySpec(byteKey)
        val kf = KeyFactory.getInstance(KeyProperties.KEY_ALGORITHM_EC)
        return kf.generatePublic(X509publicKey)
    }

    fun generateSecretRandom(len: Int): String {
        val random = SecureRandom()
        val bytes = ByteArray(len)
        random.nextBytes(bytes)
        return CryptoService.instance.toBase64(bytes)
    }

    companion object {
        private val ANDROIDKEYSTORE: String = "AndroidKeyStore"
        private val ALIAS: String = "myalias"
        val instance = CryptoService()
    }
}