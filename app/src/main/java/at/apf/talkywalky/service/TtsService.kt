package at.apf.talkywalky.service

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.UtteranceProgressListener
import at.apf.talkywalky.messaging.CompleteListener
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.util.Logger
import java.io.File
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

class TtsService(val ctx: Context) {

    private val logger = Logger(ctx, TtsService::class.java)
    private val incrementer: AtomicInteger = AtomicInteger(0)

    fun synthesize(text: String, local: String, contact: Contact, onComplete: CompleteListener) {
        val recordFile = File.createTempFile(contact.name, incrementer.getAndIncrement().toString(), ctx.cacheDir)
        var tts: TextToSpeech? = null

        tts = TextToSpeech(ctx) { status ->
            if (status == TextToSpeech.SUCCESS) {
                tts!!.setOnUtteranceProgressListener(object : UtteranceProgressListener() {
                    override fun onStart(utteranceId: String) {
                        logger.info("Starting synthesizing")
                    }

                    override fun onDone(utteranceId: String) {
                        logger.info("Finished synthesizing")
                        MediaPlayerService.getInstance(ctx).enqueue(contact, recordFile, onComplete)
                        tts!!.shutdown()
                    }

                    override fun onError(utteranceId: String) {
                        logger.info("Could not synthesize text")
                        tts!!.shutdown()
                        onComplete(false)
                    }
                })
                tts!!.setLanguage(Locale(local))
                tts!!.synthesizeToFile(text, null, recordFile, incrementer.getAndIncrement().toString())
            } else {
                logger.error("Error on init")
            }
        }
    }

    companion object {
        private var instance: TtsService? = null
        fun getInstance(ctx: Context): TtsService {
            if (instance == null) {
                instance = TtsService(ctx)
            }
            return instance!!
        }
    }
}