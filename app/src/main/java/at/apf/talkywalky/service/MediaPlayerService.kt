package at.apf.talkywalky.service

import android.content.Context
import android.media.AudioManager
import android.media.MediaPlayer
import at.apf.talkywalky.messaging.CompleteListener
import at.apf.talkywalky.model.Contact
import at.apf.talkywalky.util.Logger
import at.apf.talkywalky.util.Tuple
import java.io.File
import java.util.*
import java.util.concurrent.locks.ReentrantLock

class MediaPlayerService(private val ctx: Context) {

    private val logger = Logger(ctx, MediaPlayerService::class.java)

    private var player: MediaPlayer? = null
    private val lock: ReentrantLock = ReentrantLock(true)
    private val queue: LinkedList<Tuple<Contact, Tuple<File, CompleteListener>>> = LinkedList()
    private var lastVolume: Int = 0

    fun mute() {
        logger.info("Muting. queue size: " + queue.size)
        lock.lock()
        if (player != null) {
            player!!.stop()
            player = null
            volumeDown()
        }
        queue.forEach { tuple ->
            tuple.second.second(false)
            tuple.second.first.delete()
        }
        queue.clear()
        lock.unlock()
    }

    fun volumeUp() {
        if (PreferenceService.isVolumeUp(ctx)) {
            val audio = ctx.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
            lastVolume = audio!!.getStreamVolume(AudioManager.STREAM_MUSIC)
            audio.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                audio.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                AudioManager.FLAG_PLAY_SOUND
            )
        }
    }

    fun volumeDown() {
        if (PreferenceService.isVolumeUp(ctx)) {
            val audio = ctx.getSystemService(Context.AUDIO_SERVICE) as AudioManager?
            audio!!.setStreamVolume(AudioManager.STREAM_MUSIC, lastVolume, AudioManager.FLAG_PLAY_SOUND)
        }
    }

    @Synchronized
    fun enqueue(contact: Contact, file: File, onComplete: CompleteListener) {
        lock.lock()
        queue.add(Tuple(contact, Tuple(file, onComplete)))
        if (player == null) {
            volumeUp()

            player = MediaPlayer()
            player!!.setOnCompletionListener { completeListener() }
            player!!.setDataSource(file.absolutePath)
            player!!.prepare()
            player!!.start()

            NotificationService.getInstance(ctx).incoming(contact)
        }
        lock.unlock()
    }

    fun completeListener() {
        lock.lock()
        player!!.release()
        player = null
        val lastTalky = queue.remove()
        lastTalky.second.first.delete()
        lastTalky.second.second(true)
        if (queue.isNotEmpty()) {
            player = MediaPlayer()
            player!!.setOnCompletionListener { completeListener() }
            player!!.setDataSource(queue.peek().second.first.absolutePath)
            player!!.prepare()
            player!!.start()

            NotificationService.getInstance(ctx).incoming(queue.peek().first)
        } else {
            volumeDown()

            NotificationService.getInstance(ctx).resetDelayed()
        }
        lock.unlock()
    }

    companion object {
        private var instace: MediaPlayerService? = null

        fun getInstance(ctx: Context): MediaPlayerService {
            if (instace == null) {
                instace = MediaPlayerService(ctx)
            }
            return instace!!
        }
    }
}