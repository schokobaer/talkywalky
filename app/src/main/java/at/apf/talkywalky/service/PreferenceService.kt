package at.apf.talkywalky.service

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

class PreferenceService {

    companion object {
        private val FCM = "fcm"
        private val NAME = "name"
        private val PK = "pk"
        private val MUTE = "mute"
        private val VOLUME_UP = "volumeup"

        private fun sp(ctx: Context): SharedPreferences {
            return PreferenceManager.getDefaultSharedPreferences(ctx)
        }

        fun saveFcm(ctx: Context, fcm: String) {
            sp(ctx).edit().putString(FCM, fcm).apply()
        }

        fun getFcm(ctx: Context): String {
            return sp(ctx).getString(FCM, "")
        }

        fun saveName(ctx: Context, name: String) {
            sp(ctx).edit().putString(NAME, name).apply()
        }

        fun getName(ctx: Context): String {
            return sp(ctx).getString(NAME, "")
        }

        fun toggleMute(ctx: Context) {
            sp(ctx).edit().putBoolean(MUTE, !isMute(ctx)).apply()
        }

        fun isMute(ctx: Context): Boolean {
            return sp(ctx).getBoolean(MUTE, false)
        }

        fun savePublicKey(ctx: Context, pk: String) {
            sp(ctx).edit().putString(PK, pk).apply()
        }

        fun getPublicKey(ctx: Context): String {
            return sp(ctx).getString(PK, "")
        }

        fun isVolumeUp(ctx: Context): Boolean {
            return sp(ctx).getBoolean(VOLUME_UP, true)
        }
    }
}