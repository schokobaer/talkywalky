package at.apf.talkywalky.service

import android.content.Context
import android.media.MediaRecorder
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Toast
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.StorageReference
import java.io.File
import java.util.*
import at.apf.talkywalky.Const.Companion.STORAGE_META_TALKY_ENVELOPE
import at.apf.talkywalky.dto.TalkyEnvelope
import at.apf.talkywalky.model.Contact
import com.google.gson.Gson
import java.util.concurrent.atomic.AtomicBoolean


class RecorderService {

    private val contact: Contact
    private val ctx: Context

    private lateinit var recorder: MediaRecorder
    private val isRecording: AtomicBoolean = AtomicBoolean(false)
    private val queue: LinkedList<File> = LinkedList()
    private val firebaseStore: StorageReference = FirebaseStorage.getInstance().reference
    private var startTime: Date = Date()
    private var recordTime: Long = 0

    constructor(contact: Contact, ctx: Context) {
        this.contact = contact
        this.ctx = ctx
    }

    fun isRecording(): Boolean {
        return isRecording.get()
    }

    fun start(): Boolean {
        if (recorderLock.getAndSet(false)) {
            startTime = Date()
            initRecorder()
            recorder.prepare()
            recorder.start()
            isRecording.set(true)
            return true
        }
        return false
    }

    @Synchronized
    fun stop(): Boolean {
        if (isRecording.get()) {
            recordTime = Date().time - startTime.time
            Log.i("RECORD-TIME", recordTime.toString())
            Handler(Looper.getMainLooper()).postDelayed({ realStop() }, 700)
            return true
        }
        return false
    }

    private fun realStop() {
        recorder.stop()
        recorder.release()
        isRecording.set(false)
        recorderLock.set(true)
        val outputFile = queue.pop()

        if (recordTime < 400) {
            Log.i("RECORD-TIME", "Denied")
            outputFile.delete()
            return
        }
        Log.i("RECORD-TIME", "Accepted")

        encrypt(outputFile, contact.secret)

        val crypto = CryptoService.instance
        val receiver = Contact.fcmOrTopic(contact)
        val fan = Contact.decide(contact, TalkyEnvelope.FAN_SINGLE, TalkyEnvelope.FAN_CHANNEL)
        val signature = Contact.decide(contact,
            crypto.sign(outputFile.readBytes()),
            crypto.encrypt(crypto.sha1(outputFile.readBytes()), contact.secret))

        val envelope = TalkyEnvelope(
            senderPk = PreferenceService.getPublicKey(ctx),
            fan = fan,
            fcm =  receiver,
            signature = signature
        )
        val jsonEnvelope = Gson().toJson(envelope)

        val fileRef = firebaseStore.child("records/" + outputFile.name)
        fileRef.putFile(
            Uri.fromFile(outputFile), StorageMetadata.Builder()
            .setCustomMetadata(STORAGE_META_TALKY_ENVELOPE, jsonEnvelope)
            .build())
            .addOnSuccessListener {
                Log.i("UPLOAD", "SUCCESS")
                Toast.makeText(ctx, "Sent talky", Toast.LENGTH_SHORT).show()
                outputFile.delete()
            }
            .addOnFailureListener {
                Log.e("UPLOAD", it.message)
                it.printStackTrace()
            }

    }

    fun encrypt(file: File, key: String) {
        val encRaw = CryptoService.instance.encrypt(file.readBytes(), key)
        file.writeBytes(encRaw)
    }

    fun initRecorder() {
        recorder = MediaRecorder()
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC)
        recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        recorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        val outputFile = File.createTempFile(PreferenceService.getName(ctx) + "-" + contact.name, "", ctx.cacheDir)
        outputFile.deleteOnExit()
        queue.push(outputFile)
        recorder.setOutputFile(outputFile.absolutePath)
    }

    companion object {
        val recorderLock = AtomicBoolean(true)
    }
}