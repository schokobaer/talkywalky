package at.apf.talkywalky

import android.app.Application
import android.util.Log
import at.apf.talkywalky.service.CryptoService

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Log.i("APP_JESUS", "created")
    }

    companion object {
        fun isInitialized(): Boolean {
            return CryptoService.instance.holdsKeys()
        }
    }

}