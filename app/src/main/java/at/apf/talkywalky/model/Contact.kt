package at.apf.talkywalky.model

import java.io.Serializable

interface Contact : Serializable {
    var name: String
    var secret: String
    var lastAccess: Long
    var silent: Boolean
    var blocked: Boolean

    companion object {
        fun castWalker(contact: Contact): Walker? {
            return if (contact is Walker) contact else null
        }

        fun castChannel(contact: Contact): Channel? {
            return if (contact is Channel) contact else null
        }

        fun fcmOrTopic(contact: Contact): String {
            return castWalker(contact)?.fcm ?: castChannel(contact)?.topic ?: throw ClassNotFoundException()
        }

        fun pkOrTopic(contact: Contact): String {
            return castWalker(contact)?.pk ?: castChannel(contact)?.topic ?: throw ClassNotFoundException()
        }

        fun <T> decide(contact: Contact, caseWalker: T, caseChannel: T): T {
            return when (contact) {
                is Walker -> caseWalker
                is Channel -> caseChannel
                else -> throw ClassNotFoundException()
            }
        }
    }
}