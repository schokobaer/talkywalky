package at.apf.talkywalky.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Channel(
    @PrimaryKey @NonNull var topic: String,
    @NonNull override var name: String,
    @NonNull override var secret: String,
    @NonNull override var lastAccess: Long = System.currentTimeMillis(),

    @NonNull override var silent: Boolean = false,
    @NonNull override var blocked: Boolean = false
) : Contact, Serializable