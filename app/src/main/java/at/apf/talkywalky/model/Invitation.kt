package at.apf.talkywalky.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Invitation(
    @PrimaryKey @NonNull val secret: String,
    @NonNull val challenge: String,
    @NonNull val expireDate: Long
)