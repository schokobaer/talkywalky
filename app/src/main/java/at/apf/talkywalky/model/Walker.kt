package at.apf.talkywalky.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Walker(

    @NonNull var fcm: String,
    @PrimaryKey @NonNull var pk: String,
    @NonNull override var name: String,
    @NonNull override var secret: String,
    @NonNull override var lastAccess: Long = System.currentTimeMillis(),

    @NonNull override var silent: Boolean = false,
    @NonNull override var blocked: Boolean = false
) : Contact