package at.apf.talkywalky.dto

import at.apf.talkywalky.service.CryptoService
import com.google.gson.Gson

data class QrCodeInvite(
    val secret: String, //32 byte in base64
    val fcm: String,    // FCM or Topic
    val name: String,
    val pk: String? = null,
    val challenge: String? = null, //64 byte in base64
    val type: String = TYPE_WALKER,
    var expire: Long? = null,
    val version: Int = 1
) {

    fun serialize(): String {
        return CryptoService.instance.toBase64(Gson().toJson(this))
    }

    companion object {
        val TYPE_WALKER = "walker"
        val TYPE_CHANNEL = "channel"

        fun deserialize(obj: String): QrCodeInvite {
            return Gson().fromJson(String(CryptoService.instance.fromBase64(obj)), QrCodeInvite::class.java)
        }
    }
}