package at.apf.talkywalky.dto

data class InviteResponse(
    val fcm: String,
    val pk: String,
    val name: String,
    val challenge: String
)