package at.apf.talkywalky.dto

data class FcmRefreshRequest(
    val fcm: String,
    val pk: String,
    val signature: String
)