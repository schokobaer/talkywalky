package at.apf.talkywalky.dto

data class TalkyEnvelope(
    val filePath: String? = null,
    val senderPk: String,
    val signature: String,
    val fan: String,
    val fcm: String? = null, // FCM token or Topic
    val version: Int = 1
) {
    companion object {
        val FAN_SINGLE = "single"
        val FAN_CHANNEL = "channel"
    }
}