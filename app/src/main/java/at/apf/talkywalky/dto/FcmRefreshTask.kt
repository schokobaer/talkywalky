package at.apf.talkywalky.dto

data class FcmRefreshTask(
    val fcm: String,
    val pk: String,
    val signature: String
)