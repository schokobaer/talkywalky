package at.apf.talkywalky.dto

data class InviteResponseEnvelope(
    val receiver: String,
    val response: InviteResponse,
    val signature: String
)