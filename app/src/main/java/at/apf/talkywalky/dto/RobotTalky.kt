package at.apf.talkywalky.dto

data class RobotTalky(
    val senderPk: String,
    val fcm: String,
    val text: String,
    val locale: String,
    val fan: String,
    val signature: String,
    val version: Int = 1
)