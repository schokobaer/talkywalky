package at.apf.talkywalky.util

import android.content.Context
import android.util.Log
import java.io.File
import java.io.PrintWriter
import java.io.StringWriter
import java.text.SimpleDateFormat
import java.util.*

class Logger(val ctx: Context, val clazz: Class<*>) {

    fun debug(msg: String) {
        log(ctx, "$DEBUG ${clazz.simpleName} $msg")
        Log.d(clazz.simpleName, msg)
    }

    fun info(msg: String) {
        log(ctx, "$INFO  ${clazz.simpleName} $msg")
        Log.i(clazz.simpleName, msg)
    }

    fun warn(msg: String, error: Throwable? = null) {
        log(ctx, "$WARN  ${clazz.simpleName} $msg")
        if (error != null) {
            logStackTrace(ctx, error)
        }
        Log.w(clazz.simpleName, msg)
    }

    fun error(msg: String, error: Throwable? = null) {
        log(ctx, "$ERROR ${clazz.simpleName} $msg")
        if (error != null) {
            logStackTrace(ctx, error)
        }
        Log.e(clazz.simpleName, msg)
    }

    fun wtf(msg: String, error: Throwable? = null) {
        log(ctx, "$ERROR ${clazz.simpleName} $msg")
        if (error != null) {
            logStackTrace(ctx, error)
        }
        Log.wtf(clazz.simpleName, msg)
    }

    companion object {
        private val DEBUG = "DEBUG"
        private val INFO  = "INFO"
        private val WARN  = "WARN"
        private val ERROR = "ERROR"

        private fun file(ctx: Context): File {
            val file = File(ctx.filesDir.absolutePath + "/logfile.txt")
            if (!file.exists()) {
                file.createNewFile()
            }
            return file
        }

        @Synchronized
        fun log(ctx: Context, msg: String) {
            file(ctx).appendText("${date()} $msg\n")
        }

        @Synchronized
        fun logStackTrace(ctx: Context, error: Throwable) {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            error.printStackTrace(pw)
            log(ctx, sw.buffer.toString())
        }

        private fun date(): String {
            val sdf = SimpleDateFormat("dd.mm.yyyy HH:mm:ss")
            return sdf.format(Date())
        }

    }
}