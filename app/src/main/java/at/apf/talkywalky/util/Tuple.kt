package at.apf.talkywalky.util

data class Tuple<A, B>(
    val first: A,
    val second: B
)