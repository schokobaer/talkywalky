package at.apf.talkywalky

import android.app.Activity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.pm.PackageManager
import android.Manifest.permission.RECORD_AUDIO
import android.app.AlertDialog
import android.content.Intent
import androidx.core.content.ContextCompat
import android.util.Log
import android.widget.PopupMenu
import androidx.core.app.ActivityCompat
import at.apf.talkywalky.dao.AppDatabase
import at.apf.talkywalky.service.CryptoService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import at.apf.talkywalky.service.PreferenceService
import at.apf.talkywalky.ui.*
import java.io.File
import java.util.*
import android.content.DialogInterface
import android.text.InputType
import android.widget.EditText
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.speech.tts.TextToSpeech
import android.widget.Toast
import at.apf.talkywalky.dto.FcmRefreshRequest
import at.apf.talkywalky.model.Channel
import at.apf.talkywalky.rest.RestClient
import at.apf.talkywalky.service.SubscriptionService
import com.google.firebase.messaging.FirebaseMessaging

//import com.google.firebase.messaging.FirebaseMessaging


class MainActivity : Activity() {

    private lateinit var listAdapter: ContactsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (!checkPermission()) {
            requestPermission()
        }

        this.logo.setImageDrawable(getDrawable(R.drawable.favicon))

        this.btnScann.setOnClickListener {
            startActivity(Intent(baseContext, ScannerActivity::class.java))
        }

        this.btnMenu.setImageDrawable(getDrawable(R.drawable.ctxmenu))
        this.btnMenu.setOnClickListener {
            val menu = PopupMenu(this, btnMenu)
            menu.inflate(R.menu.menu_main)
            menu.setOnMenuItemClickListener { item ->
                when (item.itemId) {
                    R.id.menuMute -> {
                        PreferenceService.toggleMute(this)
                        return@setOnMenuItemClickListener false
                    }
                    R.id.menuInvite -> startActivity(Intent(baseContext, InviteActivity::class.java))
                    R.id.menuNewChannel -> createChannel()
                    R.id.menuSettings -> startActivity(Intent(this, SettingsActivity::class.java))
                }
                true
            }
            menu.show()
            menu.menu.findItem(R.id.menuMute).isChecked = PreferenceService.isMute(this)
        }

        listAdapter = ContactsAdapter(this)
        val mLayoutManager = LinearLayoutManager(applicationContext)
        this.list.setLayoutManager(mLayoutManager)
        this.list.setItemAnimator(DefaultItemAnimator())
        this.list.setAdapter(listAdapter)




        // Crypto tests
        val crypto = CryptoService.instance
        val file = File.createTempFile("myTest", "1", cacheDir)
        val text = "yessyessyessyessyessyessyessyess"
        val bytes = text.toByteArray()
        val key = "alkasjdfkljaslkdfjaksldfjaslkdfjlkasdjflkasjflsakjdflasjflkasdjflaksdjflasdkjflkasdfjsadlfjljs"
        file.writeBytes(bytes)
        val encrypt = crypto.encrypt(file.readBytes(), key)
        encrypt[5] = 4
        file.writeBytes(encrypt)
        file.writeBytes(crypto.decrypt(file.readBytes(), key))
        val content = file.readBytes()
        if (String(content).equals(text)) {
            Log.i("CRYPT", "yess")
        } else {
            Log.i("CRYPT", "no D:")
        }

        // Invitations
        AppDatabase.getDb(this).invitationDao().getAll()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ list ->
                val now = Date()
                list.forEach {
                    Log.i("INVITATION-DB", Date(it.expireDate).toString())
                    if (Date(it.expireDate).before(now)) {
                        Log.i("INVITATION-DB", "OLD -> Deleteing: " + Date(it.expireDate).toString())
                        AppDatabase.getDb(this).invitationDao().delete(it).subscribeOn(Schedulers.io()).subscribe()
                    }
                }
            }, {
                it.printStackTrace()
            })

        FirebaseMessaging.getInstance().subscribeToTopic("refr")
    }

    private fun createChannel() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("New Channel")
        builder.setView(R.layout.dialog_prompt)
        var input: EditText? = null
        builder.setPositiveButton(
            "Create"
        ) { dialog, which ->
            val name = input!!.text.toString()
            if (name.length < 2 || name.length > 20) {
                Toast.makeText(this, "Name must have 3-20 characters", Toast.LENGTH_SHORT).show()
                dialog.dismiss()
                return@setPositiveButton
            }
            val channel = Channel(
                SubscriptionService.generateTopicName(PreferenceService.getName(this), name),
                name,
                CryptoService.instance.generateSecretRandom(32)
            )

            AppDatabase.getDb(this).contactRepo().create(channel)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    SubscriptionService.subscribe(channel.topic)
                    dialog.dismiss()
                    //updateContactList()
                    val intent = Intent(baseContext, InviteActivity::class.java)
                    intent.putExtra(InviteActivity.EXTRA_CHANNEL, channel)
                    startActivity(intent)
                }, { err -> err.printStackTrace() })
        }
        builder.setNegativeButton(
            "Cancel"
        ) { dialog, which -> dialog.cancel() }
        val dialog = builder.show()
        input = dialog.findViewById(R.id.input)
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(RECORD_AUDIO),
            2325)
    }

    fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            applicationContext,
            RECORD_AUDIO
        ) == PackageManager.PERMISSION_GRANTED
    }

    override fun onResume() {
        super.onResume()
        instace = this
        updateContactList()
    }

    fun updateContactList() {
            AppDatabase.getDb(this)
                .contactRepo()
                .getAll()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ list ->
                    listAdapter.updateData(list.filter { c -> !c.blocked })
                }, { err ->
                    Log.e("APP-DB", err.message)
                    err.printStackTrace()
                })
    }

    override fun onPause() {
        super.onPause()
        instace = null
    }


    companion object {
        var instace: MainActivity? = null
    }

}
