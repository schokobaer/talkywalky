
import * as admin from 'firebase-admin'
import * as express from 'express'

interface InviteResponse {
    name: string
    fcm: string
    pk: string
    challenge: string
}

interface InviteResponseEnvelope {
    response: InviteResponse
    receiver: string
    signature: string
}

interface FcmUpdateEnvelope {
    fcm: string
    pk: string
    signature: string // signature of fcm
}

interface RobotTalky {
    senderPk: string
    fcm: string
    text: string
    locale: string
    fan: 'single' | 'channel'
    signature: string
    version: number
}

export const rest = express()
const messagingRouter = express.Router()

messagingRouter.post('/invite', (req, res) => {
    const invite = req.body as InviteResponseEnvelope
    console.info('INVITE: ', invite)
    // TODO: Verify signature
    return admin.messaging().sendToDevice(
        invite.receiver, 
        {
            data: {
                response: JSON.stringify(invite.response),
                action: 'invite'
            }
        }, 
        {
            contentAvailable: true,
            priority: 'high'
        }
    ).then(result => {
        res.status(204).send()
    })
})

messagingRouter.post('/refresh', (req, res) => {
    const refresh = req.body as FcmUpdateEnvelope
    const refreshData = {
        fcm: refresh.fcm,
        pk: refresh.pk,
        signature: refresh.signature
    }
    const pk = refresh.pk
    const topic = "walker." + pk.split("").filter(c => c !== "=").join(""); // remove '=' chars
    console.info('REFRESH: ', topic)
    return admin.messaging().sendToTopic(
        topic, 
        {
            data: {
                refresh: JSON.stringify(refreshData),
                action: 'refresh'
            }
        },
        {
            contentAvailable: true,
            priority: 'high'
        }
    ).then(resp => res.status(204).send())
})

messagingRouter.post('/robot', (req, res) => {
    const robotTalky = req.body as RobotTalky
    const payload = {
        data: {
            talky: JSON.stringify(robotTalky),
            action: 'robot_talky'
        }
    } as admin.messaging.MessagingPayload
    const options = {
        timeToLive: 15,
        contentAvailable: true,
        priority: 'high'
    } as admin.messaging.MessagingOptions
    console.info('ROBOT-TALKY ' + robotTalky.fcm)
    return  (robotTalky.fan === 'single') ?
        admin.messaging().sendToDevice(robotTalky.fcm, payload, options).then(reason => res.status(204).send()) :
        admin.messaging().sendToTopic(robotTalky.fcm, payload, options).then(reason => res.status(204).send())
})

rest.use('/messaging', messagingRouter)