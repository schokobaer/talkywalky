import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'
import { sendTalkyNoti } from './upload'
import { rest } from './rest'

admin.initializeApp()

export const uploadFile = functions.storage.object().onFinalize(sendTalkyNoti)
export const api = functions.https.onRequest(rest)
