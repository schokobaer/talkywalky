import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin'

interface TalkyEnvelope {
    filePath?: string
    senderPk: string
    signature: string
    fan: 'single' | 'channel'
    fcm?: string
    version: number
}

export const sendTalkyNoti = (obj: functions.storage.ObjectMetadata, ctx: functions.EventContext) => {
    if (obj.metadata && obj.metadata['envelope']) {
        const recEnvelope = JSON.parse(obj.metadata['envelope'])
        const respEnvelope = {
            filePath: obj.name,
            senderPk: recEnvelope.senderPk,
            signature: recEnvelope.signature,
            fan: recEnvelope.fan,
            fcm: recEnvelope.fcm,
            version: recEnvelope.version
        } as TalkyEnvelope
        const payload = {
            data: {
                talky: JSON.stringify(respEnvelope),
                action: 'talky'
            }
        } as admin.messaging.MessagingPayload
        const options = {
            timeToLive: 15,
            contentAvailable: true,
            priority: 'high'
        } as admin.messaging.MessagingOptions
        console.info('UPLOADED: REC: ' + recEnvelope.fcm + ' / ID: ' + obj.name)
        return  (respEnvelope.fan === 'single') ?
            admin.messaging().sendToDevice(recEnvelope.fcm, payload, options).then(reason => checkForOldFiles()) :
            admin.messaging().sendToTopic(recEnvelope.fcm, payload, options).then(reason => checkForOldFiles())
    }
    return true
}

/**
 * Checks all files in the records folder. 
 * If the file is older then 2 minutes it gets deleted.
 */
const checkForOldFiles = () => {
    console.info('CHECK OLD FILES')
    admin.storage().bucket().getFiles({directory: 'records/'}).then(files => {
        const now: number = new Date().getTime()
        files[0].forEach(f => {
            const obj = JSON.parse(JSON.stringify(f.metadata))
            const created = new Date(obj.timeCreated).getTime()
            if (((now - created) / (1000 * 60)) > 2 && obj.name.length > 'records/'.length) {
                f.delete()
                .then(resp => {
                    console.info('Deleted file: ' + obj.name)
                })
                .catch(e => {
                    console.warn(e)
                })
            }
        })
    }).catch(e => {
        console.warn(e)
    })
}